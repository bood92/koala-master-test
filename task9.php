<?php
function task9($str)
{
    $result = [];
    foreach (explode(',', $str) as $item) {
        if (preg_match('/^\d+\.\d+$/', trim($item))) {
            $result[] = $item;
        }
    }

    return implode(',', $result);
}

$str = '10 , 22, 2.1, 2.5, 10.10, 500.1, 77, 10.11.12.13';
echo task9($str);

