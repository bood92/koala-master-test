<?php

function task10(array $array)
{
    for ($i = 0; $i < count($array); $i++) {
        for ($j = 0; $j < count($array[$i]); $j++) {
            $array[$i][$j] = intval($array[$i][$j]);
        }
    }

    return $array;
}

$array = [
    [
        '1',
        '2'
    ],
    [
        '1',
        '2'
    ],
    [
        '1',
        '2'
    ]
];


var_dump($array);
echo "<hr/>";
var_dump(task10($array));
