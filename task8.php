<?php
/*
8. Представити масив у форматі JSON (без використання функції json_encode)
$arr = array('a' => 1, 'b' => 2, 'c' => 3, 'd' => 4, 'e' => 5);
*/

function toJSON(array $array) {
    $result = "";

    foreach ($array as $key => $item) {
        $result .= '"' . $key . '":' . $item;

        if (next($array) == true) {
            $result .= ",";
        }
    }

    return '{' . $result . '}';
}

$arr = array('a' => 1, 'b' => 2, 'c' => 3, 'd' => 4, 'e' => 5);
echo toJSON($arr);